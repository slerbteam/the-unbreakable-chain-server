package com.someluigi.tuc.server.item.crafting;

import com.someluigi.tuc.server.item.Item;

public class ItemStick extends Item {

	/**
	 * @serial
	 */
	private static final long serialVersionUID = 1L;
	
	public String name = "Wooden Stick";
	public String subtext = "A thin shaft made from wood...";
	
	
	@Override
	public int getBlitemID() {
		return 10000; // the first item
	}

}
