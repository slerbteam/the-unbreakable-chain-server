package com.someluigi.tuc.server.item;

import java.io.Serializable;

import com.someluigi.tuc.server.player.FakePlayer;
import com.someluigi.tuc.server.world.Chunk;

public abstract class Item implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8774951311873962610L;
	
	/*
	 * This is the Blitem ID
	 * Start with 10'000
	 * This class is abstract and defines it as -1.
	 */
	public abstract int getBlitemID();
	
	/*
	 * This is the metadata. This should always be 0 (no meta) upon Item creation.
	 * However, at runtime it may be changed.
	 */
	public int meta = 0;
	
	/*
	 * This is the name.
	 */
	public String name = "Abstract Class Item (Unnamed)";
	
	/*
	 * This is the subtext.
	 */
	public String subtext = "";
	
	/*
	 * Item Subclasses may define other arbitrary data, since the Item object
	 * is serialised directly, it may contain arbitrary data without the need
	 * of a TileData definition.
	 */
	
	/*
	 * Returns whether the (fake)player can drop this Item or not.
	 * Default value is true if not overriden.
	 * Use FakePlayer.isFake() to see if the player is fake.
	 */
	public boolean canDrop(FakePlayer p, Chunk c, int x, int y, int z) {
		return true; 
	}
	
	/*
	 * This happens when a (fake)player drops the item.
	 * Returning false will cancel ONLY the spawning of the EntityItem.
	 * This is useful for spawning your own EntityItem.
	 * Use FakePlayer.isFake() to see if the player is fake.
	 */
	public boolean onDrop(FakePlayer p, Chunk c, int x, int y, int z) {
		return true;
	}
	
}
