package com.someluigi.tuc.server.block;

import java.io.Serializable;

public class BlockRecord implements Serializable {

	
	public int id, meta, tiledata;
	// Block ID, Block Meta, TID of TileEntity in the chunk of this block
	
	/**
	 * @serializable
	 */
	private static final long serialVersionUID = -6186798468403468717L;

	protected void _construct(int id, int meta, int tiledata) {
		this.id = id;
		this.meta = meta;
		this.tiledata = tiledata;
	}
	
	public BlockRecord(int id, int meta, int tiledata) {
		_construct(id, meta, tiledata);
	}

	public BlockRecord(int id, int meta) {
		_construct(id, meta, -1);
	}

	public BlockRecord(int id) {
		_construct(id, 0, -1);
	}
	
	
}
