package com.someluigi.tuc.server.block;

import com.someluigi.tuc.server.events.BlockBreakCause;
import com.someluigi.tuc.server.player.Player;
import com.someluigi.tuc.server.world.Chunk;

public interface IBlockReponder {

	public boolean isOverPlacable = false;
	// can a block be placed to replace this?
	
	public boolean canBreak(BlockRecord b, Chunk c, Player p, BlockBreakCause cause);
	public boolean canPlace(BlockRecord b, Chunk c, Player p);
	// CAN? Pre-Event Methods
	
	public void useBlock(BlockRecord b, Chunk c, Player p);
	// DO! Event Methods
	
}
