package com.someluigi.tuc.server.events;

public enum EntityDamageCause {
	PLAYER_MELEE, PLAYER_RANGED, ENTITY_MELEE, ENTITY_RANGED, BURN, EXPLOSION, PROGRAMMATICAL;
	
	public boolean wasPlayer() {
		return (this==PLAYER_MELEE) || (this==PLAYER_RANGED);
	}
	
	public boolean wasEntity() {
		return (this==ENTITY_MELEE) || (this==ENTITY_RANGED);
	}
	
	public boolean wasRanged() {
		return (this==PLAYER_RANGED) || (this==ENTITY_RANGED);
	}
	
	public boolean wasMelee() {
		return (this==PLAYER_MELEE) || (this==ENTITY_MELEE);
	}
	
}
