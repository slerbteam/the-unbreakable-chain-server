package com.someluigi.tuc.server.events;

public enum BlockBreakCause {
	PLAYER, FAKEPLAYER, PROGRAMMATICAL, EXPLOSION, BURN;
	
	
	/*
	 * PLAYER: Block broke by player
	 * FAKEPLAYER: Block broke by fakeplayer i.e. Robot
	 * PROGRAMMATICAL: Block broke by program - no reason
	 * EXPLOSION: An explosion broke it!
	 * BURN: The block burnt.  
	 */
	
}
