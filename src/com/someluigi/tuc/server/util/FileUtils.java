package com.someluigi.tuc.server.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

public class FileUtils {
	
	public static void download(String pURL, String pFile) {
		
		try {

	        /*
	         * Get a connection to the URL and start up
	         * a buffered reader.
	         */
	        //long startTime = System.currentTimeMillis();
	 
	 
	        URL url = new URL(pURL);
	        url.openConnection();
	        InputStream reader = url.openStream();
	 
	        /*
	         * Setup a buffered file writer to write
	         * out what we read from the website.
	         */
	        FileOutputStream writer = new FileOutputStream(pFile);
	        byte[] buffer = new byte[153600];
	        //int totalBytesRead = 0;
	        int bytesRead = 0;
	 
	        while ((bytesRead = reader.read(buffer)) > 0)
	        {  
	           writer.write(buffer, 0, bytesRead);
	           buffer = new byte[153600];
	         //  totalBytesRead += bytesRead;
	        }
	 
	        //long endTime = System.currentTimeMillis();
	        
	        writer.close();
	        reader.close();
	     }
	     catch (MalformedURLException e)
	     {
	        e.printStackTrace();
	     }
	     catch (IOException e)
	     {
	        e.printStackTrace();
	     }
		
	}
	
	public static String readFile(String file, String csName)
            throws IOException {
    Charset cs = Charset.forName(csName);
    return readFile(file, cs);
	}

	public static String readFile(String file, Charset cs)
	            throws IOException {
	    // No real need to close the BufferedReader/InputStreamReader
	    // as they're only wrapping the stream
	    FileInputStream stream = new FileInputStream(file);
	    try {
	        Reader reader = new BufferedReader(new InputStreamReader(stream, cs));
	        StringBuilder builder = new StringBuilder();
	        char[] buffer = new char[8192];
	        int read;
	        while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
	            builder.append(buffer, 0, read);
	        }
	        return builder.toString();
	    } finally {
	        // Potential issue here: if this throws an IOException,
	        // it will mask any others. Normally I'd use a utility
	        // method which would log exceptions and swallow them
	        stream.close();
	    }        
	}
	
}
