package com.someluigi.tuc.server.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class EasySerialize {
	
	public static boolean saveObj(String file, Object obj) {
		
		try {
	        FileOutputStream fileOut = new FileOutputStream("./" + file);
	        ObjectOutputStream out = new ObjectOutputStream(fileOut);
	        out.writeObject(obj);
	        out.close();
	        fileOut.close();
	      } catch(IOException i) {
	        i.printStackTrace();
	      }
		
		return true;
	}
	
	public static Object loadObj(String file) {
		Object o;
		
		try {
			FileInputStream fileIn = new FileInputStream("./" + file);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			o = in.readObject();
			in.close();
			fileIn.close();
		} catch(IOException i) {
			i.printStackTrace();
			return false;
		} catch(ClassNotFoundException c) {
			System.out.println("Unserialization Problem: Class not found");
			c.printStackTrace();
			return false;
		}
		
		return o;
	}
	
}
