package com.someluigi.tuc.server.world;

import java.io.File;
import java.util.HashMap;

import com.someluigi.tuc.server.util.EasySerialize;

public class ChunkManager {
	private static HashMap<String, Chunk> Chunks = new HashMap<String, Chunk>();
	
	
	
	
	
	private static boolean _loadChunk(String chunkname) {
		File f = new File("./data/chunks/" + chunkname + ".jvser"); 
		if(f.exists() && !f.isDirectory()) {
			Chunks.put(chunkname, (Chunk)EasySerialize.loadObj("data/chunks/" + chunkname + ".jvser"));
			
			return true;
		}
		
		return false;
	}
	
	public static void _saveChunk(String chunkname) {
		EasySerialize.saveObj("data/chunks/" + chunkname + ".jvser", Chunks.get(chunkname));
	}
	
	public static void _freeChunk(String chunkname) {
		Chunks.put(chunkname, null);
		System.gc(); // Garbage Collect!
	}
	
	public static void unloadChunk(String chunkname) {
		if (isChunkLoaded(chunkname)) {
			_saveChunk(chunkname);
			_freeChunk(chunkname);
		}
	}
	
	public static void _newChunk(String chunkname) {
		Chunk c = new Chunk();
		c.name = chunkname;
		Chunks.put(chunkname, c);
	}
	
	public static boolean isChunkLoaded(String chunkname) {
		return (Chunks.get(chunkname)!=null);
	}
	
	
	
	public static Chunk getChunk(String chunkname) {
		if (isChunkLoaded(chunkname)) {
			return Chunks.get(chunkname);
		} else {
			if (! _loadChunk(chunkname)) {
				return null;
			}
			return Chunks.get(chunkname);
		}
	}
}
