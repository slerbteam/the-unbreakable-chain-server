package com.someluigi.tuc.server.world;

import java.io.Serializable;
import java.util.Hashtable;

public class Chunk implements Serializable {
	
	/**
	 * @DESC ?
	 */
	private static final long serialVersionUID = 1L;
	
	
	public String name;
	public String slug;
	public int maxX = 12;
	public int maxY = 12;
	public Hashtable<Point3D, String> blocks = new Hashtable<Point3D, String>();
	
	public String chk_lt, chk_rt, chk_up, chk_dn = "";
		// the borders of this chunk lead nowhere
	
	
	
	
}
