package com.someluigi.tuc.server.world;

import java.util.HashMap;

public interface IWorldGenerator {
	
	public Chunk doChunk(String name, String slug);
	public void configure(HashMap<String, Object> context);
	
}
