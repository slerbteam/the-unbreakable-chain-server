package com.someluigi.tuc.server.world;

import java.io.Serializable;

public class Point3D implements Serializable {
	
	/**
	 * @serializable
	 */
	private static final long serialVersionUID = 5611447394715125262L;
	
	public double cX;
	public double cY;
	public double cZ;
	
	
	@Override
	public String toString() {
		return "(" + this.cX + ", " + this.cY + ", " + this.cZ + ")";
	}
	
	@Override
	public int hashCode() {
		return (int) (this.cX*this.cY*this.cZ);
	}
	
	public boolean equals(Point3D dp3) {
		return (dp3.cX==this.cX) && (dp3.cX==this.cX) && (dp3.cX==this.cX);
	}
	
	public Point3D(int x, int y, int z) {
		this.cX = x;
		this.cY = y;
		this.cZ = z;
	}
	
}
