package com.someluigi.tuc.server.player;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.someluigi.tuc.server.util.EasySerialize;

public class Player extends FakePlayer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String src, name, techname;
	public int slot;
	public Map<String, Object> simpleMap = new HashMap<String, Object>();
	
	public Player(String src, String name, int slot) {
		techname = src + ';' + name + ';' + slot;
	}
	
	public void load() {
		
	}
	public void save() {
		EasySerialize.saveObj("a_player.player.ser", this);
	}
	
	public boolean isFake() {
		return false;
	}
	
}
