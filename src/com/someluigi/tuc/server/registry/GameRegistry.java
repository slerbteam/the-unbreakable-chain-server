package com.someluigi.tuc.server.registry;

import java.util.HashMap;
import java.util.LinkedList;

import com.someluigi.tuc.server.block.IBlockReponder;
import com.someluigi.tuc.server.item.Item;

public class GameRegistry {
	
	public HashMap<Integer, Class<? extends Item>> items = new HashMap<Integer, Class<? extends Item>>();
	public HashMap<Integer, Class<? extends IBlockReponder>> blocks = new HashMap<Integer, Class<? extends IBlockReponder>>();
	public LinkedList<Recipe> recipes = new LinkedList<Recipe>();
	
	public int addRecipe(BlitemStack[] result, Object[] oa) {
		Recipe r = new Recipe(result, oa);
		recipes.add(r);
		return recipes.lastIndexOf(r); // return the Recipe, maybe there's a
		// reason for doing so.
	}
	
	
	
}
