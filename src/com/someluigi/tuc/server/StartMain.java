package com.someluigi.tuc.server;

import java.io.PrintStream;
import java.util.logging.Logger;

public class StartMain {

	public static PrintStream stdOut=System.out;
	public static Logger out;
	
	public static int build = 0;
	public static String version = "0.0.000";
	
	public static String[] args;
	
	/**
	 * @param args
	 */
	public static void main(String[] arguments) {
		
		StartMain.args = arguments;
		
		stdOut.println("Starting TUC Server...");
		out = Logger.getLogger("TUC");
		
		out.info("Version " + version + " (build #" + build + ")");
		
		try {
			HttpServer.start();
		} catch (Exception e) {
			out.severe("Exception in HttpServer...");
			e.printStackTrace();
		}
		
	}

}
