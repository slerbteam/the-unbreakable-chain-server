package com.someluigi.tuc.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import org.simpleframework.http.Address;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.Server;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;

import com.someluigi.tuc.server.player.Player;
import com.someluigi.tuc.server.util.EasySerialize;

public class HttpServer implements Container {
	
	public static Connection connection;
	public PrintStream stdOut = System.out;

	public static void start() throws Exception {
		Container container = new HttpServer();
		Server server = new ContainerServer(container);
		connection = new SocketConnection(server);
		SocketAddress address = new InetSocketAddress(Integer.valueOf(StartMain.args[0]));
		
		connection.connect(address);
		
		//connection.close();
	}
	
	public static void stop() throws IOException {
		connection.close();
	}

	@Override
	public void handle(Request req, Response res) {
		try {
			PrintStream body = res.getPrintStream();
			long time = System.currentTimeMillis();
			   
			res.setValue("Server", "TUCServer/"+StartMain.version+" (theuchain)");
			res.setDate("Date", time);
			res.setDate("Last-Modified", time);
			
			
			Address a = req.getAddress();
			
			// notes: domain is null
			
			
			String[] seg = a.getPath().getSegments();
			if (seg[0].equalsIgnoreCase("res") || seg[0].equalsIgnoreCase("html")) {
				
				File rf = new File("../" + a.getPath().getPath(0));
				
				if (! rf.exists()) {
					res.setCode(404);
					res.setValue("Content-Type", "text/html");
					body.println("<h2>404</h2><br>The resource was not available. This is not intended to be user-browsed. ");
					body.close();
					return;
				}
				
				String ext = a.getPath().getExtension();
				if (ext.equalsIgnoreCase("png")) {
					res.setValue("Content-Type", "image/png");
				} else if (ext.equalsIgnoreCase("gif")) {
					res.setValue("Content-Type", "image/gif");
				} else if (ext.equalsIgnoreCase("jml")) {
					res.setValue("Content-Type", "text/html");
					
				} else {
					res.setCode(500);
					res.setValue("Content-Type", "text/html");
					body.println("<h2>500 Internal TUC Server Error</h2><br>The resource does not have a known MIME-Type, cannot transfer properly. Extension: " + ext);
					body.close();
					return;
				}
				
				// a resource was called!
				// the resources are usually binary files, so we can't copy them
				// as text
				
				// but we will do this with text files anyway, for simplicity
				
				// Get a Stream from the file
				FileInputStream reader = new FileInputStream(rf);
				
		        byte[] buffer = new byte[153600];
		        int bytesRead = 0;
		 
		        while ((bytesRead = reader.read(buffer)) > 0)
		        {  
		        	// copy the bytes from the filein stream to the httpout stream
		        	body.write(buffer, 0, bytesRead);
		        	buffer = new byte[153600];
		        }
		        
		        body.close(); // Close Stream
		        reader.close(); // Close Stream
		        return; // Exit from HTTP Processing
			}
			
			String addr = a.toString();
			if (addr.equalsIgnoreCase("/ser")) {
				Player p = new Player("slm", "AUser", 1);
				p.save();
				body.println("Serializing player");
				stdOut.println("Stopping the server...");
			}
			
			if (addr.equalsIgnoreCase("/stop")) {
				body.println("Stopping Server");
				body.close();
				HttpServer.stop();
			}
			
			if (addr.equalsIgnoreCase("/deser")) {
				Player p = (Player) EasySerialize.loadObj("a_player.player.ser");
				body.println("Loaded player " + p.techname);
				
			}
			body.println("Accessing URL: " + req.getAddress());
			body.close();
	      } catch(Exception e) {
	        e.printStackTrace();
	      }
	}

}
