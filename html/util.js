///////////////////////////////////
//
// ===TUC Source JS===
// @use closurecompiler
//
// ==ClosureCompiler==
// @compilation_level SIMPLE_OPTIMIZATIONS
// @output_file_name util.c.js
// @formatting single_quotes
// ==/ClosureCompiler==
//
///////////////////////////////////




window.log = function(msg) {
	if (debug==1) {
		console.log(msg);
	}
}


var KEY_LEFT = 37;
var KEY_UP = 38;
var KEY_RIGHT = 39;
var KEY_DOWN = 40;
var actKeys = new Array();
var entKeys = new Array();


function rand(min, max) {
	return Math.floor(Math.random() * max) + min;
}


function pDef(e) {
	
	
	if (window.event) {
		// horrible IE, go away :P
		window.event.cancelBubble = true;
		if (window.event.preventDefault) {
		window.event.preventDefault();
		}
	} else {
		// hello there standards, you are welcome here
		// even if you don't support some of these functions
		if (e.preventDefault) {
			e.preventDefault();
		}
		if (e.stopPropagation) {
	  		e.stopPropagation();
	  	}
	}

  e.returnValue = false;
} 


function keyDn(key, event) {
   //return false;
   pd = 0;
   
	if (key==KEY_LEFT) {
		pd=1;
	}
	if (key==KEY_RIGHT) {
		pd=1;
	}
	if (key==KEY_DOWN) {
		pd=1;
	}
	if (key==KEY_UP) {
		pd=1;
	}
	
if ((pd==1) && (event!="button")) {
	window.log('pDef @ dn');
	pDef(event);
	return false;
}	
	
	//if (evt==KEY_UP) {
	//	entities['player'].dirMovePx(3);
	// event.preventDefault();
	//}   
   
   return true;
   
   //if (entKeys[evt.keyCode]) {
   //	entKeys[evt.keyCode].del();
	//	entKeys.pop(evt.keyCode)
	//	window.log("--- Dual Press?")
   //}
   //entKeys[evt.keyCode] = new EntityText("key" + evt.keyCode).setVisible(true).setText("Keypress");
}

function keyUp(key, event) {
	//entKeys[evt.keyCode].del();
	//entKeys.pop(evt.keyCode);

pd = 0;	
	window.log('@ up');
	if (entities['player'] instanceof Entity) {
		if (key==KEY_LEFT) {
			entities['player'].left();
			pd=1;
		}
		if (key==KEY_RIGHT) {
			entities['player'].right();
			pd=1;
		}
		if (key==KEY_DOWN) {
			entities['player'].reverse();
			pd=1;
		}
		//if (evt==KEY_UP) {
		//	entities['player'].dirMovePx(3);
		// event.preventDefault();
		//}
	}
	
if ((pd==1) && (event!="button")) {
	window.log('pDef @ up');
	pDef(event);
	return false;
} else {
	window.log('NO pDef @ up, pd=' + pd + ', event=' + event);
}
	
	return true;
}

/*
document.onkeydown=function(event){
	//return keyDn(key.keyCode);

	if(window.event){
		// IE
		keyDn(window.event.keyCode, window.event);
	} else {
		// Other
		keyDn(holder=event.which, event);
	} 
	
	return false;
	 
};
document.onkeyup=function(event){
	//return keyUp(key.keyCode);
	
	if(window.event){
		// IE
		keyUp(window.event.keyCode, window.event);
	} else {
		// Other
		keyUp(holder=event.which, event);
	} 
	
	return false;
};
*/










function keydnhnd(e){
    	
window.log('key down ' + e.keyCode);    	
    	
    	document.body.scrollTop=220;

		e = e || window.event;    	
    	
    	if (actKeys[e.keyCode]==undefined) {
	    	actKeys[e.keyCode]=1;
	      keyDn(e.keyCode, e);
	      window.log("keyDown")
      } else {
      	window.log("fake keyDown")
      	keyDn(e.keyCode, e); // pass the event so duplicates still get the "prevent Default"
      								 // treatment
      }
      if (e.doStop) {
			return false;
		}	
    }

function keyuphnd(e){

		document.body.scrollTop=220;

		e = e || window.event;      	
    	
    	if (actKeys[e.keyCode]==1) {
    		actKeys[e.keyCode]=undefined;
      	keyUp(e.keyCode, e);
      	//window.log("keyUp")
      	if (e.doStop) {
				return false;
			}	
      }
    }
    
    
function onLoadMethod() {
if (! window.addEventListener) {
    if (window.attachEvent) {
        //alert('Warning: some features may be incompatible with Internet Explorer!');
        //alert('(Microsoft does not follow standards properly...)');
        //alert('If you experience a bug using IE please state that you use IE in the bug report!');
        // USING INTERNET EXPLORER --- THATS A PROBLEM!
        
window.document.attachEvent("keydown", keydnhnd);
window.document.attachEvent('keyup', keyuphnd);      

alert('Warning: Internet Explorer Event Handling used! Do not report key-based bugs with this!');  
        
    } else {
    	alert('Warning: neither addEventListener (proper Event support), or even attachEvent (Microsoft/IEs rubbish copy) is available on your system.');
    }
} else {
	window.document.addEventListener("keydown",
    keydnhnd,
false);
window.document.addEventListener('keyup',
    keyuphnd,
false);
}   

resetFrame();
window.setTimeout('gameTick();', 1000);

}
    
    
    
    
    

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}












var entities = new Array();
var entityreg = new Array();
var entitytypes = new Array(); // used to register so that upstream can easily implement new ClientEntities without adding transtream words for it, holds constructors (this['Entity'] == function Entity)  



function gbid(id) {
    return document.getElementById(id);
}

function oldremClass(obj, cl) {
obj.className = obj.className.replace(' ' + cl, '');
}

function hasClass(ele,cls) {
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

function remClass(ele,cls) {
        if (hasClass(ele,cls)) {
            var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
            ele.className=ele.className.replace(reg,' ');
        }
}

window.setTimeout("document.body.scrollTop=220;", 1000);
var seld = 1;




var errorManager = {
	report: function (type, message) {

	},
	severe: function (type, message) {
		//alert('[SEVERE] The game must now be quit.');
		resetFrame();
		gbid('gameview').innerHTML = '<span style="color: red;">[SEVERE]</span> <span style="color: orange;">' + type + ' - ' + message + '</span>';

		window.setTimeout('gbid("gameview").innerHTML += "<br><br>The game must now exit.";', 2500);
		window.setTimeout('location="../menu/";', 6000);

		requestManager.active=false
		//location.href = 'error.php?err=' + encodeURIComponent(type + '<!--s-->', message);
		//location = '../menu/';
	}
}





var requestManager = {
	httpr: "",
    outgoing: [],
    outgone: true,
    delay: -1,
    arr: [],
    startReq: 0,
    timer: -1,
	active: true,
    count: function () {
        return arr.count();
    },
    comm: function () {
        var d = new Date();
        var t = d.getTime();
        this.startReq = t;
        window.alert('not ready');
    },
    RequestManagerRequestStatusUpdateEvent: function () {
        


		if (this.httpr.readyState==4) {
		
		if (this.httpr.status!=200) {
		// e.o PROBLEM!
			errorManager.report('EConnectionError', 'Connection Failed.');
return;
		}
		
		this.outgoing = []

        var d = new Date();
        var t = d.getTime();
        this.startReq = t;
        this.delay = this.startReq - t;
		
		doArr = this.httpr.responseText.split('<!--s-->');

		var length = doArr.length,
		resp = null;
		for (var i = 0; i < length; i++) {
			resp = doArr[i].split('<!--p-->');
			
			switch (resp[0]) {
			
			case 'malformed':
				errorManager.severe('ERemoteCommunicationError', 'Server says... Unknown Communication Method: ' + resp[1].toString());
			break;

			case 'fullWorld':
//alert('fw... ' + resp[1]);
				xArr = resp[1].split('<!--x-->');
				xlength = xArr.length
//alert('x' + xlength)
				for (var xi = 0; xi < xlength; xi++) {
					yArr = xArr[xi].split('<!--y-->');
					ylength = yArr.length;
//alert('y' + ylength)
					for (var yi = 0; yi < ylength; yi++) {
						setBlock(xi+1, yi+1, yArr[yi]);
					}
				}
			break;
			
			case 'spawnEntity':
				new entitytypes[resp[1]](resp[2]); // create Entity type %1 with name %2
				requestManager.queue(['spawnedEntity', resp[2]]);
			break;

         case 'entityUpdate':
         	
         break;
         
         case 'despawnEntity':
         	entities[resp[1]].del();
         	requestManager.queue(['despawnedEntity', resp[1]]);
         break;
         
         case 'js':
         	eval(resp[1]);
         break;

			default:
				//alert("(game error) Unimplemented Communication Method: " + resp.toString());
				errorManager.severe('ELocalCommunicationError', 'Unknown Communication Method: ' + resp[0].toString() + '<br><br>(raw:)<br>' + resp.join('&lt;sep&gt;'));
			}

		}


		}



    },
    queue: function (request) {
        this.arr.push(request.join('<!--p-->'));
    },
    ogtStart: function () {
        if (this.timer==-1) {
            this.timer = window.setTimeout("requestManager.outgo();", 2000);
        }
    },
    ogtStop: function () {
        if (this.timer != -1) {
            window.clearInterval(this.timer);
            this.timer=-1;
        }
    },
    outgo: function () {

		if (this.active==false) {
			return;
		}
        if (this.outgoing.length==0) {
            this.outgoing = this.arr;
            this.arr = [];
        }

		requestManager.httpr.onreadystatechange = relay;

		this.httpr.open("POST", "comm_layer.php", true);
		this.httpr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		this.httpr.send("queue=" + encodeURIComponent(this.outgoing.join("<!--s-->")));
    }

}

if (window.XMLHttpRequest)
{
	requestManager.httpr = new XMLHttpRequest();
} else {
	requestManager.httpr = new ActiveXObject("Microsoft.XMLHTTP");
};


function relay() {
requestManager.RequestManagerRequestStatusUpdateEvent();
}






function println(s) {
    gbid('console').innerHTML += s + '<br>';
}

function sel(n) {
    remClass(gbid('hb' + seld), 'select');
	gbid('hb' + n).className += ' select';
	seld = n;
}

function gui(b) {
	if (b==1) {
		gbid('main').style.display = 'none';
		gbid('gui').style.display = 'block';
	} else {
		gbid('gui').style.display = 'none';
		gbid('main').style.display = 'block';
	}
}

function doReq(a) {
	alert('{ request ' + a + ' }');
}


function cleanFrame() {
fref().innerHTML="";
}


function resetFrame() {
// reset
gbid('gameview').innerHTML = '<span class="center"><iframe id="gameframe" class="gameframe" frameBorder=0 width="100%" height="100%">[SEVERE ERROR] No iFrame Support. iFrame Support is required so resources can be safely and reliably unloaded, giving you better performance. Modern browsers support iFrames, so please update.</iframe></span>';
}

function cleanFrame() {

}

function fdref() {
return gbid('gameframe').contentWindow.document;
}

function fref() {
return gbid('gameframe').contentWindow.document.body;
}

function fgbid(id) {
return gbid('gameframe').contentWindow.document.getElementById(id);
}

function loadWorld() {
cleanFrame();
requestManager.queue(['fullWorld']);
requestManager.outgo();
}


function setBlock(x, y, texture) {
//alert('setting ' + x + ', ' + y + ' to ' + texture);

delBlock(x, y)

	bl = fdref().createElement('img');
	bl.id = 'blockx' + x.toString() + 'y' + y.toString();
	bl.style.position = "absolute";
	bl.style.left = ((x-1)*16).toString()+"px";
	bl.style.top  = ((y-1)*16).toString()+"px";
	fref().appendChild(bl);

fgbid('blockx' + x.toString() + 'y' + y.toString()).src = texture;



}

function delBlock(x, y) {
if (fgbid('blockx' + x.toString() + 'y' + y.toString())!=null) {
b=fgbid('blockx' + x.toString() + 'y' + y.toString())
b.parentNode.removeChild(b)
}
}


function BaseEntity(name) {
	this.name=name;
	this.offX=0
	this.offY=0
	this.posX=0
	this.posY=0
	this.visible = false;
	this.ele = null;
	this.type = "BaseEntity"; // make the type ENTITY --- subclasses will override this
	this.del = function() {
		delEntity(name);
		entities.pop(name)
		entityreg.pop(this.regid)
	}
	this.setOffset = function (x, y) {
		this.offX = x;
		this.offY = y;
		if (this.offEvt) {
			if (!this.offEvt(x, y)) {
				return;
			}
		}
		return this
	}
	this.setPos = function(x, y) {
		this.posX = x
		this.posY = y
		this.ele.style.left   = (this.posX-this.offX).toString()+"px";
		this.ele.style.bottom = (this.posY-this.offY).toString()+"px";
		if (this.posEvt) {
			if (!this.posEvt(x, y)) {
				return;
			}
		}
		return this
	}
	this.setVisible = function(v) {
		if (this.visibleEvt) {
			if (!this.visibleEvt(v)) {
				return;
			}
		}
		this.ele.style.display = "block";
		return this
	}
	this.setZIndex = function(z) {
		this.ele.style.zIndex = z;
		return this
	}
	entities[name] = this
	entityreg.push(this)
	this.regid = entityreg.indexOf(this)
}



function Entity(name) {
	
	// Entity
	// as of 15/01/2013, this class now contains:
	// BaseEntity (inherits with apply), FacableEntity (integrated), MultiStateFacableEntity (integrates)
	BaseEntity.apply(this, [name]);
	this.type = "Entity";	
	
	newEntity(name, 'img');
	this.ele = fgbid('entity_' + name);
	this.setSprite = function (spr) {
	this.ele.src=spr;
	}
	
	this.faceSprites = ['', '', '', '']
	this.stateSprites = [];
	this.facing=0
	this.turnTo = function (face) {
		this.setSprite(this.faceSprites[face]);
		this.facing = face
		return this
	}
	this.right = function() {
		++this.facing
		if (this.facing>3) {
			this.facing=0
		}
		if (this.facing<0) {
			this.facing=3
		}
		this.turnTo(this.facing)
	}
	this.left = function() {
		--this.facing
		if (this.facing>3) {
			this.facing=0
		}
		if (this.facing<0) {
			this.facing=3
		}
		this.turnTo(this.facing)
	}
	this.reverse = function() {
		--this.facing
		if (this.facing>3) {
			this.facing=0
		}
		if (this.facing<0) {
			this.facing=3
		}
		--this.facing
		if (this.facing>3) {
			this.facing=0
		}
		if (this.facing<0) {
			this.facing=3
		}
		this.turnTo(this.facing)
	}
	this.dirMovePx = function(px) {
		switch (this.facing) {
			case 0:
				this.setPos(this.posX, this.posY+px);
			break;
			case 1:
				this.setPos(this.posX+px, this.posY);
			break;
			case 2:
				this.setPos(this.posX, this.posY-px);
			break;
			case 3:
				this.setPos(this.posX-px, this.posY);
			break;
		}
	}
	this.boundDMP = function(px) {
		minX=0 + this.offX
		maxX=192 - this.offX
		minY=0 + this.offY
		maxY=192 - this.offY
		
		switch (this.facing) {
			case 0:
				if (this.posY+px<maxY) {
					this.setPos(this.posX, this.posY+px);
				}
			break;
			case 1:
				if (this.posX+px<maxX) {
					this.setPos(this.posX+px, this.posY);
				}
			break;
			case 2:
				if (this.posY-px>minY) {
					this.setPos(this.posX, this.posY-px);
				}
			break;
			case 3:
				if (this.posX-px>minX) {
					this.setPos(this.posX, this.posY+px);
				}
			break;
		}
	}	
	
	this.setState = function (state) {
		this.faceSprites = this.stateSprites[state];
		this.turnTo(this.facing);
	}
	
	this.setStateSprites = function (state, sprites) {
		this.stateSprites[state] = sprites;
	}
	
	
}
entitytypes["Entity"] = Entity;







function TextEntity(name) {
	BaseEntity.apply(this, [name]);
	this.type = "TextEntity";
	newEntity(name, 'span');
	this.ele = fgbid('entity_' + name);
	this.text = '';
	this.setText = function(txt) {
		this.ele.innerHTML = htmlEntities(txt);
		this.text = txt;
		return this
	} 
	this.setCol = function (col) {
		this.ele.style.color = col;
		return this
	}
	this.setCol('#00ff00');
	this.setText('');
}
entitytypes["TextEntity"] = TextEntity;








function entityExists(name) {
	return fgbid('entity_' + name)!=null;
}

function newEntity(name, typee) {
	if (! entityExists(name)) {
		e = fdref().createElement(typee);
		e.id = 'entity_' + name;
		e.style.position = "absolute";
		e.style.display = "none";
		fref().appendChild(e);
	} else {
		errorManager.severe('ESyncError.EntitySync', 'The server specified an entity creation request for an entity that already existed!');
	}
}


function delEntity(name) {
	if (entityExists(name)) {
		e=fgbid('entity_' + name)
		e.parentNode.removeChild(e)
	} else {
		errorManager.severe('ESyncError.EntitySync', 'The server specified an entity deletion request for an entity that did not exist!');
	}
}





var gameTicks=0;

function gameTick() {
	document.body.scrollTop=220;
	++gameTicks; // increment the Game Tick Number
	
	
	if (entities['player']!=undefined) {
		p = entities['player'];
		if (actKeys[KEY_UP]==1) {
			p.dirMovePx(3);
		}
	}
	
	if (entities['npc']!=undefined) {
		npc = entities['npc'];
		action = rand(1, 4);
		
		
		if (gameTicks%30==0) {
			// only turn every 30 ticks
		switch (action) {
			case 1:
			npc.left();
			break;
			case 2:
			npc.right();
			break;
			case 3:
			npc.boundDMP(4);
			break;
			case 4:
			npc.reverse();
			break;
		}
	} else {
		npc.boundDMP(4);
	}
	}
	
if (debug==1) {
	window.log('[TUC Client Tick] Tick ' + gameTicks + ' performed');
}	
	
	gameticktimer=window.setTimeout('gameTick();', gametickintval);
}

var gameticktimer=-1
var gametickintval = 100
var debug = -1

function startDebug() {
	
		window.log('[TUC Debugger] Welcome to the Debugger!');
		window.log('[TUC Debugger] This is for debugging only!');
		window.log('[TUC Debugger] You may receive spam in the console!');
		debug=1
		window.stopGameticks = function() {
			if (gameticktimer!=-1) {
				window.clearTimeout(gameticktimer)
				gameticktimer=-1
				window.log('[TUC Debugger] Ticking Stopped');
			} else {
				window.log('[TUC Debugger] Failed to stop ticks (are they stopped already?)');
			}
		}
		window.log('[debug method] stopGameticks() - stops Game Timer');
		window.tickGame = function(gti) {
			gametickintval=gti
			gameTick();
			window.log('[TUC Debugger] tickGame - Ticking started');
		}
		window.log('[debug method] tickGame(interval in milliseconds) - starts Game Timer');
		
		//window.log('[debug method] ');
		window.log('[TUC Debugger] *** This is for debug purposes only. ***');
	}

guiCtx = undefined;

function openGui(name, src, html) {
	guiCtx = new Object(); // make a new gui context from an object
	guiCtx.name = name; // Give a name
	
	guiCtx.src = src; // Attach source
	guiCtx.html = html; // attach the html
	eval.apply(guiCtx, [src]); // put the code onto the gooey context
	
	guiCtx.preShow();
	
}
